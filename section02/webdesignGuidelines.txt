Font
====
good font web source is:
    - https://www.google.com/fonts
        usage: copy website
        <link href="https://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
        font-family: "Raleway", sans-serif;
    - https://www.fontsquirrel.com
    
Golden ratio typography:
========================
    - https://www.pearsonidied.com/typography/
    - https://www.gridlover.net/try

Web Colors:
===========
    - https://www.color.adobe.com/create/color-wheel/
    - https://checkmycolors.com
    - https://colorlovers.com
    - https://colrd.com

Icons for Web
=============
    - https://fortawesome.github.io/Font-Awesome/
    - http://www.typicons.com/

Images for web:
==============
    - http://www.imgopt.com/
    - https://kraken.io/web-interface
    - https://tinypng.com/
    - http://jpeg-optimizer.com/
    - http://optipng.sourceforge.net/
    - http://www.advsys.net/ken/util/pngout.htm
    - http://freecode.com/projects/jpegoptim

Whitespacing:
=============
    - http://demo.themezilla.com/hanna/
    - http://www.madebysofa.com/
    - http://www.informationarchitects.jp/en/

Getting insoiration:
====================
    - http://themeforest.net/
    - http://www.themezilla.com/
    - https://www.behance.net/
    - https://dribbble.com/
    - https://www.thebestdesigns.com/
    - http://siiimple.com/
    - https://onepagelove.com/
    - http://www.cssdsgn.com/
    - https://bestwebsite.gallery/
    - http://styleboost.com/
    - http://www.siteinspire.com/
    - http://www.admiretheweb.com/blog/