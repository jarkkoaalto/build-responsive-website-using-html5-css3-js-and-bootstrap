# Build-Responsive-Website-Using-HTML5-CSS3-JS-And-Bootstrap

About this course
Learn HTML5, CSS3, JavaScript, jQuery and Bootstrap framework by building a modern looking responsive website.

Course content

###### Section 02: Web Design Guidelines
###### Section 03: Getting Started With HTML
###### Section 04: Styling With CSS
###### Section 05: Working With JavaScript & JQuery
###### Section 06: Bootstrap Responsive Framework
###### Section 07: Building Real Responsive Website
###### Section 08: Adding JavaScript & JQuery Effects
###### Section 09: Make Website Perfect For Responsive Layout
###### Section 10: Optimization, Validation And Testing of Website
